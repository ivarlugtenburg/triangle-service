package nl.teamrockstars.triangle.service.impl;

import nl.teamrockstars.triangle.domain.Triangle;
import nl.teamrockstars.triangle.domain.TriangleType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class TriangleServiceTest {

    private TriangleService triangleService;

    @BeforeEach
    void setUp() {
        triangleService = new TriangleService();
    }

    @Test
    void getTriangleTypeForSideLengths_negativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> {
            triangleService.getTriangleWithSides(986, -675, 4635);
        });
    }

    @Test
    void getTriangleTypeForSideLengths_zero() {
        assertThrows(IllegalArgumentException.class, () -> {
            triangleService.getTriangleWithSides(0, 654, 348);
        });
    }

    @Test
    void getTriangleTypeForSideLengths_EQUILATERAL() {
        Triangle triangle = triangleService.getTriangleWithSides(13, 13, 13);
        assertEquals(TriangleType.EQUILATERAL, triangle.getType());
        assertEquals(List.of(13, 13, 13), triangle.getSideLengths());
    }

    @Test
    void getTriangleTypeForSideLengths_ISOSCELES() {
        Triangle triangle = triangleService.getTriangleWithSides(56, 888, 56);
        assertEquals(TriangleType.ISOSCELES, triangle.getType());
        assertEquals(List.of(56, 888, 56), triangle.getSideLengths());
    }

    @Test
    void getTriangleTypeForSideLengths_SCALENE() {
        Triangle triangle = triangleService.getTriangleWithSides(130, 446, 6);
        assertEquals(TriangleType.SCALENE, triangle.getType());
        assertEquals(List.of(130, 446, 6), triangle.getSideLengths());
    }
}