package nl.teamrockstars.triangle.it;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import nl.teamrockstars.triangle.domain.Triangle;
import nl.teamrockstars.triangle.domain.TriangleType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;


@MicronautTest
class TriangleIT {

    private static final String TRIANGLE_TYPE_API = "/api/v1/triangle";

    @Inject
    private EmbeddedServer server;

    private HttpClient httpClient;

    @BeforeEach
    void setUp() throws MalformedURLException {
        URL url = new URL("http://" + server.getHost() + ":" + server.getPort());
        httpClient = HttpClient.create(url);
    }

    @Test
    public void testGetTriangle_Equilateral() throws JsonProcessingException {
        Triangle expectedTriangle = Triangle.of(1, 1, 1);
        String uri = TRIANGLE_TYPE_API + "/1/1/1";
        String actualTriangle = httpClient.toBlocking().retrieve(HttpRequest.GET(uri), String.class);
        assertNotNull(actualTriangle);
        assertEquals(new ObjectMapper().writeValueAsString(expectedTriangle), actualTriangle);
    }

    @Test
    public void testGetTriangle_Isosceles() throws JsonProcessingException {
        Triangle expectedTriangle = Triangle.of(6, 1, 1);
        String uri = TRIANGLE_TYPE_API + "/6/1/1";
        String actualTriangle = httpClient.toBlocking().retrieve(HttpRequest.GET(uri), String.class);
        assertNotNull(actualTriangle);
        assertEquals(new ObjectMapper().writeValueAsString(expectedTriangle), actualTriangle);
    }

    @Test
    public void testGetTriangle_Scalene() throws JsonProcessingException {
        Triangle expectedTriangle = Triangle.of(1, 8, 14);
        String uri = TRIANGLE_TYPE_API + "/1/8/14";
        String actualTriangle = httpClient.toBlocking().retrieve(HttpRequest.GET(uri), String.class);
        assertNotNull(actualTriangle);
        assertEquals(new ObjectMapper().writeValueAsString(expectedTriangle), actualTriangle);
    }

    @Test
    public void testGetType_Equilateral() {
        String uri = TRIANGLE_TYPE_API + "/1/1/1/type";
        String triangleType = httpClient.toBlocking().retrieve(HttpRequest.GET(uri), String.class);
        assertNotNull(triangleType);
        assertEquals(TriangleType.EQUILATERAL.getDisplayName(), triangleType);
    }

    @Test
    public void testGetType_Isoscoles() {
        String uri = TRIANGLE_TYPE_API + "/6/1/1/type";
        String triangleType = httpClient.toBlocking().retrieve(HttpRequest.GET(uri), String.class);
        assertNotNull(triangleType);
        assertEquals(TriangleType.ISOSCELES.getDisplayName(), triangleType);
    }

    @Test
    public void testGetType_Scalene() {
        String uri = TRIANGLE_TYPE_API + "/1/8/14/type";
        String triangleType = httpClient.toBlocking().retrieve(HttpRequest.GET(uri), String.class);
        assertNotNull(triangleType);
        assertEquals(TriangleType.SCALENE.getDisplayName(), triangleType);
    }
}
