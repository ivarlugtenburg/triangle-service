package nl.teamrockstars.triangle.domain;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class TriangleTest {

    @Test
    void testOf_negativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> {
            Triangle.of(46, 123, -5657);
        });
    }

    @Test
    void testOf_zero() {
        assertThrows(IllegalArgumentException.class, () -> {
            Triangle.of(90, 678, 0);
        });
    }

    @Test
    void testOf_EQUILATERAL() {
        Triangle triangle = Triangle.of(42, 42, 42);
        assertEquals(TriangleType.EQUILATERAL, triangle.getType());
        assertEquals(List.of(42, 42, 42), triangle.getSideLengths());
    }

    @Test
    void testOf_ISOSCELES() {
        Triangle triangle = Triangle.of(123146, 11, 11);
        assertEquals(TriangleType.ISOSCELES, triangle.getType());
        assertEquals(List.of(123146, 11, 11), triangle.getSideLengths());
    }

    @Test
    void testOf_SCALENE() {
        Triangle triangle = Triangle.of(7487, 63, 786);
        assertEquals(TriangleType.SCALENE, triangle.getType());
        assertEquals(List.of(7487, 63, 786), triangle.getSideLengths());
    }
}