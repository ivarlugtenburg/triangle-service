package nl.teamrockstars.triangle.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class TriangleHelperTest {

    @Test
    void testDetermineTriangleType_negativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> {
            TriangleHelper.determineTriangleType(-4, 4, 4);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            TriangleHelper.determineTriangleType(123, -35, 4);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            TriangleHelper.determineTriangleType(6756, 4, -67);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            TriangleHelper.determineTriangleType(-34, -654, -7447);
        });
    }

    @Test
    void testDetermineTriangleType_zero() {
        assertThrows(IllegalArgumentException.class, () -> {
            TriangleHelper.determineTriangleType(0, 4, 4);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            TriangleHelper.determineTriangleType(0, 0, 5);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            TriangleHelper.determineTriangleType(150, 4, 0);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            TriangleHelper.determineTriangleType(0, 0, 0);
        });
    }

    @Test
    void testDetermineTriangleType_EQUILATERAL() {
        TriangleType triangleType = TriangleHelper.determineTriangleType(4, 4, 4);
        assertEquals(TriangleType.EQUILATERAL, triangleType);
    }

    @Test
    void testDetermineTriangleType_ISOSCELES() {
        TriangleType triangleType = TriangleHelper.determineTriangleType(4, 4, 6);
        assertEquals(TriangleType.ISOSCELES, triangleType);
    }

    @Test
    void testDetermineTriangleType_SCALENE() {
        TriangleType triangleType = TriangleHelper.determineTriangleType(4, 5, 6);
        assertEquals(TriangleType.SCALENE, triangleType);
    }
}