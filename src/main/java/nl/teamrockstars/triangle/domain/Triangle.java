package nl.teamrockstars.triangle.domain;

import java.util.Arrays;
import java.util.List;


public class Triangle {

    private List<Integer> sideLengths;
    private TriangleType type;


    private Triangle(List<Integer> sideLengths, TriangleType type) {
        this.sideLengths = sideLengths;
        this.type = type;
    }

    public static Triangle of(int lengthA, int lengthB, int lengthC) {
        List<Integer> sideLengths = Arrays.asList(lengthA, lengthB, lengthC);
        TriangleType triangleType = TriangleHelper.determineTriangleType(lengthA, lengthB, lengthC);

        return new Triangle(sideLengths, triangleType);
    }

    public List<Integer> getSideLengths() {
        return sideLengths;
    }

    public TriangleType getType() {
        return type;
    }
}
