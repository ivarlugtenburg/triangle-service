package nl.teamrockstars.triangle.domain;

import java.util.HashSet;
import java.util.Set;


public final class TriangleHelper {

    public static TriangleType determineTriangleType(final int lengthA, final int lengthB, final int lengthC) {
        if (lengthA <= 0 || lengthB <= 0 || lengthC <= 0) {
            throw new IllegalArgumentException("Triangle side lengths must be bigger than 0");
        }

        final Set<Integer> sideLengths = new HashSet<>();
        sideLengths.add(lengthA);
        sideLengths.add(lengthB);
        sideLengths.add(lengthC);

        final int uniqueSideLengths = sideLengths.size();
        if (uniqueSideLengths == 1) {
            return TriangleType.EQUILATERAL;
        }

        if (uniqueSideLengths == 2) {
            return TriangleType.ISOSCELES;
        }

        return TriangleType.SCALENE;
    }
}
