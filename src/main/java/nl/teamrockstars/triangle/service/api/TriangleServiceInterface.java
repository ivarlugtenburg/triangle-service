package nl.teamrockstars.triangle.service.api;

import nl.teamrockstars.triangle.domain.Triangle;


public interface TriangleServiceInterface {

    Triangle getTriangleWithSides(int lengthA, int lengthB, int lengthC);
}
