package nl.teamrockstars.triangle.service.impl;

import nl.teamrockstars.triangle.domain.Triangle;
import nl.teamrockstars.triangle.service.api.TriangleServiceInterface;

import javax.inject.Singleton;


@Singleton
public class TriangleService implements TriangleServiceInterface {

    @Override
    public Triangle getTriangleWithSides(int lengthA, int lengthB, int lengthC) {
        return Triangle.of(lengthA, lengthB, lengthC);
    }
}
