package nl.teamrockstars.triangle.controller;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

import javax.inject.Singleton;


@Produces
@Singleton
@Requires(classes = {IllegalArgumentException.class, ExceptionHandler.class})
public class ExceptionResponseHandler implements ExceptionHandler<IllegalArgumentException, HttpResponse<String>>{

    @Override
    public HttpResponse<String> handle(HttpRequest request, IllegalArgumentException exception) {
        return HttpResponse.badRequest(exception.getMessage());
    }
}
