package nl.teamrockstars.triangle.controller.impl;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.QueryValue;
import nl.teamrockstars.triangle.controller.api.TriangleControllerInterface;
import nl.teamrockstars.triangle.domain.Triangle;
import nl.teamrockstars.triangle.domain.TriangleType;
import nl.teamrockstars.triangle.service.api.TriangleServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;


@Controller("/api/v1/triangle")
public class TriangleController implements TriangleControllerInterface {

    private static final Logger LOGGER = LoggerFactory.getLogger(TriangleController.class);

    private TriangleServiceInterface triangleService;


    public TriangleController(final TriangleServiceInterface triangleService) {
        this.triangleService = triangleService;
    }

    @Override
    @Produces(MediaType.APPLICATION_JSON)
    @Get("/{lengthA}/{lengthB}/{lengthC}")
    public Mono<HttpResponse<Triangle>> getTriangleWithSides(@QueryValue int lengthA,
                                                             @QueryValue int lengthB,
                                                             @QueryValue int lengthC) {

        LOGGER.info("GET /api/v1/triangle/*/*/*");

        Triangle triangle = triangleService.getTriangleWithSides(lengthA, lengthB, lengthC);
        HttpResponse<Triangle> httpResponse = HttpResponse.ok(triangle);

        return Mono.just(httpResponse);
    }

    @Override
    @Produces(MediaType.TEXT_PLAIN)
    @Get("/{lengthA}/{lengthB}/{lengthC}/type")
    public Mono<HttpResponse<TriangleType>> getTriangleWithSidesType(@QueryValue int lengthA,
                                                                     @QueryValue int lengthB,
                                                                     @QueryValue int lengthC) {

        LOGGER.info("GET /api/v1/triangle/*/*/*/type");

        Triangle triangle = triangleService.getTriangleWithSides(lengthA, lengthB, lengthC);
        HttpResponse<TriangleType> httpResponse = HttpResponse.ok(triangle.getType());

        return Mono.just(httpResponse);
    }
}
