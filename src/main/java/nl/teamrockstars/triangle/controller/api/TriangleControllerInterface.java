package nl.teamrockstars.triangle.controller.api;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.QueryValue;
import nl.teamrockstars.triangle.domain.Triangle;
import nl.teamrockstars.triangle.domain.TriangleType;
import reactor.core.publisher.Mono;


public interface TriangleControllerInterface {

    Mono<HttpResponse<Triangle>> getTriangleWithSides(@QueryValue int lengthA, @QueryValue int lengthB, @QueryValue int lengthC);
    Mono<HttpResponse<TriangleType>> getTriangleWithSidesType(@QueryValue int lengthA, @QueryValue int lengthB, @QueryValue int lengthC);
}
