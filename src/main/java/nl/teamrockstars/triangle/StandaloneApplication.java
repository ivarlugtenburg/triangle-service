package nl.teamrockstars.triangle;

import nl.teamrockstars.triangle.domain.TriangleHelper;
import nl.teamrockstars.triangle.domain.TriangleType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StandaloneApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(StandaloneApplication.class);

    public static void main(String[] args) {
        if (args.length != 3) {
            LOGGER.error("Please provide exactly 3 triangle side lengths...");
            return;
        }

        try {
            int lengthA = Integer.parseInt(args[0]);
            int lengthB = Integer.parseInt(args[1]);
            int lengthC = Integer.parseInt(args[2]);

            TriangleType triangleType = TriangleHelper.determineTriangleType(lengthA, lengthB, lengthC);
            LOGGER.info("Triangle type is: " + triangleType.getDisplayName());
        } catch (NumberFormatException e) {
            LOGGER.error("Input arguments should be numeric");
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.getMessage());
        }
    }
}